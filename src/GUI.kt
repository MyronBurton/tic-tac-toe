import java.awt.GridLayout
import javax.swing.JFrame

/**
 * Creates a GUI tilted "Tic Tac Toe" with 9 buttons in a square grid
 */
class GUI : JFrame("Tic Tac Toe") {
    init {
        setBounds(0, 0, 600, 600)
        isResizable = false
        layout = GridLayout(3,3)
        gameSquares.forEach{ add(it.button) }
        isVisible = true
    }
}
import java.awt.Font
import javax.swing.JButton

/**
 * Creates a game square
 */
class GameSquare {
    // button for use with GUI. Also stores the value of the cell under .text
    val button = JButton()
    var textIsSet = false

    /**
     * Creates button with action listener for clicks. Font is large for easy viewing
     */
    init {
        button.addActionListener { stateMachine.move(this) }
        button.font = Font("Arial", 0, 150)
    }

    /**
     * returns text value of square
     */
    fun getText(): String = button.text

    /**
     * sets text and sets textIsSet to true
     */
    fun setText(mark: String) {
        button.text = mark
        textIsSet = true
    }

    /**
     * resets text of square to "" and sets textIsSet to false
     */
    fun reset() {
        button.text = ""
        textIsSet = false
    }
}
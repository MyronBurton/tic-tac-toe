import java.io.PrintWriter
import javax.swing.SwingUtilities

var gameSquares: ArrayList<GameSquare> = ArrayList()    // list of all tic tac toe squares
val stateMachine = StateMachine()   // keeps track of state and runs any bot players
val output = ArrayList<String>()    // used to keep order of output statements for assignment
val winCases = arrayOf(
        WinCase(0,1,2),
        WinCase(0,3,6),
        WinCase(0,4,8),
        WinCase(1,4,7),
        WinCase(3,4,5),
        WinCase(2,4,6),
        WinCase(2,5,8),
        WinCase(6,7,8)) // the 8 different possible win states
const val numberOfSims = 100  // how many simulations to run
var gui: GUI? = null

data class WinCase(val first: Int, val second: Int, val third: Int)

/**
 * Launch point of program. Args specify number of players.
 * -nogui and 2 will run the program with two AI without a gui.
 * All other possible arguments will run one AI and show the gui
 * allowing a game against a bot. No arguments means no bots and
 * the gui shows.
 */
fun main (args: Array<String>) {
    // first lines of output for assignment
    output.add("Myron Burton")
    output.add("Player 1 won 0" +
            " games Player 2 won 0" +
            " games there were 0 ties")
    // add first bot if necessary
    if (args.isNotEmpty()) {
        stateMachine.aiList.add(Player("O"))
    }
    // add second bot and return building no gui
    if (args.isNotEmpty() && (args[0] == "2" || args[0].toLowerCase() == "-nogui")) {
        stateMachine.aiList.add(Player("X"))
        stateMachine.runAI()
        return
    }
    // build gui if 1 or fewer bots
    gui = GUI()
}

/**
 * Keeps track of the state of the game, resets, and runs bots
 */
class StateMachine {
    private var xWins = 0; private var oWins = 0; private var stalemates = 0; private var winner = 0
    private var gameNumber = 0; private var isTurnX = true; private var isGameOver = false
    private val moveList: ArrayList<Int> = ArrayList()    // history of moves taken. Resets each game.
    val aiList = ArrayList<Player>()

    /**
     * Build out game squares
     */
    init {
        for (x in 0..8)
            gameSquares.add(GameSquare())
    }

    /**
     * Prints a full history of the game
     */
    private fun fullPrint() {
        val boardPrinter = BoardPrinter()
        moveList.forEach { boardPrinter.printWithNextMove(it) }
    }

    /**
     * Prints game number, player 1 first move, player 2 first move, and winner
     * in that order. Winner = 0 if the game was a draw
     * @param firstGame prints turn by turn history of board and a heading if true
     */
    private fun printGame(firstGame: Boolean) {
        if (firstGame) {
            fullPrint()
            output.add("Game\tPlayer1FirstMove\tPlayer2FirstMove\tWinner")
        }
        output.add("${gameNumber}\t${moveList[0] + 1}\t\t\t" +
                "${moveList[1] + 1}\t\t\t" + winner)
    }

    /**
     * Checks to see if a win case is satisfied
     * @param winCase case to check
     */
    private fun checkWin(winCase: WinCase) : Boolean {
        if (gameSquares[winCase.first].getText() != ""
                && gameSquares[winCase.first].getText() == gameSquares[winCase.second].getText()
                && gameSquares[winCase.second].getText() == gameSquares[winCase.third].getText()) {
            return true
        }
        return false
    }

    /**
     * Checks the game to see if a win or draw has happened
     */
    private fun checkGameOver() {
        // check normal win (three of same type in a row)
        winCases.forEach {
            if (checkWin(it)) {
                isGameOver = true
                // x wins
                if (gameSquares[it.first].getText() == "X") {
                    xWins++
                    winner = 1
                    return
                }
                // o wins
                else {
                    oWins++
                    winner = 2
                    return
                }
            }
        }
        // check for stalemate
        gameSquares.forEach {
            if (!it.textIsSet) {
                isGameOver = false
                return
            }
        }
        // only reachable if stalemate
        isGameOver = true
        stalemates++
        winner = 0
    }

    /**
     * runs two bots against each other up to the number of simulations
     */
    fun runAI() {
        while (gameNumber < numberOfSims) {
            if (isGameOver) resetGame()
            // bot 1 runs first since it is X
            aiList[1].move()
            if (!isGameOver) aiList[0].move()
        }
    }

    /**
     * Resets the game. X starts next game. Clears move history
     */
    private fun resetGame() {
        printGame(firstGame = (gameNumber++ == 0))
        // prints to a file if the number of simulations has been reached
        if (gameNumber == numberOfSims) {
            fullPrint()
            output[1] = "Player 1 won $xWins" +
                    " games Player 2 won $oWins" +
                    " games there were $stalemates ties"
            val file = PrintWriter("./output.txt")
            output.forEach { file.println(it) }
            file.close()
        }
        // clean up game
        gameSquares.forEach { it.reset() }
        isGameOver = false
        moveList.removeAll(moveList)
        isTurnX = true
    }

    /**
     * mark a square with x if isTurnX or o if !isTurnX
     * @param gameSquare which square to mark
     */
    fun mark(gameSquare: GameSquare) {
        // mark appropriate value
        if (isTurnX) gameSquare.setText("X")
        else gameSquare.setText("O")
        gameSquare.textIsSet = true
        // record move and switch turns. Check if game over
        moveList.add(gameSquares.indexOf(gameSquare))
        isTurnX = !isTurnX
        checkGameOver()
    }

    /**
     * Player safe move. Will run bot if one is available
     * @param gameSquare square to mark
     */
    fun move(gameSquare: GameSquare) {
        //
        if (!gameSquare.textIsSet && !isGameOver) mark(gameSquare)
        else if (isGameOver) resetGame()
        // run bot in half a second. Delay is introduced to make it more natural
        if (aiList.size == 1 && !isTurnX && !isGameOver) {
            SwingUtilities.invokeLater {
                Thread.sleep(500)
                aiList[0].move()
            }
        }
    }
}